#include <iostream>
#include <math.h>
#include <string>
#include <iomanip>

using namespace std;

const int N=4;
double A[N][N]={};
void imprimir(double matriz[N][N]){
    cout<<"La matriz A es: "<<endl;
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            cout<<A[i][j]<<"\t";
        }
    cout<<""<<endl;
    }
}
void posibleDominantePorFilas(double matriz[N][N]){
    double suma2;
    int contador2=0;
    int posicion[N]={};
    for (int i = 0; i < N; i++){
        double max=fabs(A[i][0]);
        for (int j = 0; j < N; j++){
            if (fabs(A[i][j])>max){
                max=fabs(A[i][j]);
            }
        suma2=suma2+A[i][j];
        }
        for (int j = 0; j < N; j++){
            if(max==fabs(A[i][j])){
                posicion[i]=j;
            }
        }
    double nomax=fabs(suma2-max);
    if (nomax<fabs(max)){
            contador2++;
    }
    suma2=0;
    cout<<contador2<<endl;
    }
    bool imposible=false;
    bool imposible2=false;
    if (contador2<N){
        imposible=true;
    }else{
        for (int t = 0; t < N; t++){
            for (int i = t+1; i < N; i++){
                if (posicion[t]==posicion[i]){
                    imposible2=true;
                }
            }
        }
    }
    if (imposible){
        cout<<"La matriz no es ni puede ser dominante por filas. No todas las filas cumplen la condici\242n."<<endl;
    }else if(imposible2){
        cout<<"La matriz no es ni puede ser dominante por filas. Dos o mas filas tienen el elemento dominante en la misma posici\242n."<<endl;
    }else{
        cout<<"la matriz puede convertirse en dominante permutando columnas"<<endl;
    }  
}
void dominantePorFilas(double matriz[N][N]){
    double suma;
    int contador;
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            suma=suma+matriz[i][j];
        }
    double nodiag=fabs(suma-matriz[i][i]);
    if (nodiag<fabs(matriz[i][i])){
            contador++;
        }
    suma=0;
    }
    if (contador==N){
        cout<<"la matriz es dominante por filas"<<endl;
    }
    else{
        cout<<"la matriz no es dominante por filas"<<endl;
        posibleDominantePorFilas(matriz);
    }
    contador=0;  
}

int main(){
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout<<"introduce el valor de la fila "<<i+1<<" columna "<<j+1<<"= ";
            cin>>A[i][j];
        }  
    }
    imprimir(A);
    dominantePorFilas(A);
    return 0;      
}