#include <iostream>
#include <math.h>
#include <string>
#include <iomanip>

using namespace std;
//declaracion de la constante global N y de la matriz A
const int N=4;
double A[N][N]={};
//Esta función imprime matrices cuadradas de orden N
void imprimir(double matriz[N][N]){
    std::cout<<"La matriz es: "<<std::endl;
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            std::cout<<matriz[i][j]<<"\t";
        }
    std::cout<<""<<std::endl;
    }
}
//Esta función se realizó para intentar un abordaje alternativo del problema. se descartó. Realiza la traspuesta de una matriz dada.
/*void transponer(double matriz[N][N]){
    double matrizt[N][N]={};
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            matrizt[j][i]=matriz[i][j];
        }
    }
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            matriz[i][j]=matrizt[i][j];
        }     
    }  
}*/
//Esta función se encarga de permutar columnas.
void permutarColumnas(double matriz1[N][N],int col1,int col2,int posiciones[N]){
    if(0<=col1 && col1<N && 0<=col2 && col2<N){
        double matriz2[N][N]={};
        cout<<"Se cambiar\240 la columna "<<col1+1<<" por la columna "<<col2+1<<"."<<endl;
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                if (j==col1){
                    matriz2[i][col2]=matriz1[i][j];
                }else if(j==col2){
                    matriz2[i][col1]=matriz1[i][j];
                }else{
                    matriz2[i][j]=matriz1[i][j];
                } 
            }
        }
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                matriz1[i][j]=matriz2[i][j];
            }     
        }
        double suma2;
        int contador2;
        for (int i = 0; i < N; i++){
            double max=fabs(A[i][0]);
            for (int j = 0; j < N; j++){
                if (fabs(A[i][j])>max){
                    max=fabs(A[i][j]);
                }
            suma2=suma2+A[i][j];
            }
            for (int j = 0; j < N; j++){
                if(max==fabs(A[i][j])){
                    posiciones[i]=j;
                }
            }
        double nomax=fabs(suma2-max);
        if (nomax<fabs(max)){
            contador2++;
        }
        suma2=0;
    }
    }else{
        cout<<"valores no v\240lidos de columna";
    }   
}
//Esta función evalúa la matriz no dominante y transforma en dominante si es necesario.
void posibleDominantePorFilas(double matriz[N][N]){
    double suma2=0;
    int contador2=0;
    int posicion[N]={};
    for (int i = 0; i < N; i++){
        double max=fabs(A[i][0]);
        for (int j = 0; j < N; j++){
            if (fabs(A[i][j])>max){
                max=fabs(A[i][j]);
            }
        suma2=suma2+fabs(A[i][j]);
        }
        for (int j = 0; j < N; j++){
            if(max==fabs(A[i][j])){
                posicion[i]=j;
            }
        }
    double nomax=fabs(suma2-max);
    if (nomax<fabs(max)){
            contador2++;
    }
    suma2=0;
    }
    bool imposible=false;
    bool imposible2=false;
    if (contador2<N){
        imposible=true;
    }else{
        for (int t = 0; t < N; t++){
            for (int i = t+1; i < N; i++){
                if (posicion[t]==posicion[i]){
                    imposible2=true;
                }
            }
        }
    }
    if (imposible){
        cout<<"La matriz no es ni puede ser dominante por filas. No todas las filas cumplen la condici\242n."<<endl;
    }else{
        if(imposible2){
            cout<<"La matriz no es ni puede ser dominante por filas. Dos o mas filas tienen el elemento dominante en la misma posici\242n."<<endl;
        }else{
            cout<<"la matriz puede convertirse en dominante conmutando columnas"<<endl;
            int posicion2[N]={};
            for (int i = 0; i < N; i++){
                posicion[i];
                if (i!=posicion[i]){
                    permutarColumnas(matriz,i,posicion[i],posicion);
                    posicion[posicion[i]]=posicion[i];
                    posicion[i]=i;
                }else{
                    posicion2[i]=posicion[i];                
                }
            }
        imprimir(matriz);
        }
    }  
}
//Esta función criba matrices dominantes de no dominantes.
void dominantePorFilas(double matriz[N][N]){
    double suma;
    int contador;
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            suma=suma+matriz[i][j];
        }
    double nodiag=fabs(suma-matriz[i][i]);
    if (nodiag<fabs(matriz[i][i])){
            contador++;
        }
    suma=0;
    }
    if (contador==N){
        std::cout<<"la matriz es dominante por filas"<<std::endl;
    }
    else{
        std::cout<<"la matriz no es dominante por filas"<<std::endl;
        posibleDominantePorFilas(matriz);
    }
    contador=0;  
}
//Función principal que ordena las funciones anteriores para la ejecución del programa. También pide los valores de A.
int main(){
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout<<"introduce el valor de la fila "<<i+1<<" columna "<<j+1<<"= ";
            std::cin>>A[i][j];
        }  
    }
    imprimir(A);
    dominantePorFilas(A);
    return 0;      
}