#include <iostream>
#include <iomanip>
#include <math.h>
#include <set>
#include <vector>
#include <map>
#include<queue>
#include <string>
//liberia para gestionar archivos de texto
#include <fstream>
using namespace std;

vector<vector<string>> enlaces();
bool buscador(string ordena);
int caminos(string origen, string destino,vector<vector<string>> grafo);
int caminos2(string origen, string destino,vector<vector<string>> grafo);
int main(){
    bool o;
    string origen;
    bool d;
    string destino;
    cout<<"introduce el ID del ordenador de origen"<<endl;
    cin>>origen;
    o=buscador(origen);
    if(o){
        cout<<"introduce el ID del ordenador de destino"<<endl;
        cin>>destino;
        d=buscador(destino);
        if (d){
            vector<vector<string>> grafo=enlaces();
            int distancia=caminos(origen, destino, grafo);
            if (distancia==0){
                cout<<"No hay camino"<<endl;
            }else{
                cout<<"Longitud del camino= "<<distancia<<endl;
            }
            
            
            
        }else{
            cout<<"El ID: "<<destino<<" no se encuentra en el archivo"<<endl;
        }
    }else{
        cout<<"El ID: "<<origen<<" no se encuentra en el archivo"<<endl;
    }
  
   
    return 0;      
}
int caminos(string origen, string destino,vector<vector<string>> grafo){
    int distancia;
    string w;
    set<string> visitados;
    queue<string> cola;
    map<string,int> orden;
    orden.insert(pair<string,int>(origen,0));
    visitados.insert(origen);
    cola.push(origen);
    while (!cola.empty()){
        string &v=cola.front();
        cola.pop();
        visitados.insert(v);
        for(int i=0; i<grafo.size(); i++){
            if (v==grafo.at(i).at(0)){
                w=grafo.at(i).at(1);
                if (visitados.find(w)==visitados.end()){//significa que w no está en visitados.
                    visitados.insert(w);
                    cola.push(w);
                    orden.insert(pair<string,int>(w,orden[v]+1));
                }  
            }
        }
    }
    for (auto iter=orden.begin() ; iter !=orden.end(); iter++){
        if (iter->first==destino){
            distancia=iter->second;
        }
    }
    return distancia;
}
vector<vector<string>> enlaces(){
    ifstream archivo;
    string conexion;
    string origen;
    string destino;
    vector<vector<string>> uniones;
    archivo.open("redes.txt",ios::in);
    if (archivo.fail()){
        cout<<"No se pudo abrir";
        exit(1);
    }
    while (getline(archivo, conexion)){
        vector<string> unionn;
        int posicion=conexion.find("\t");
        origen=conexion.substr(0,posicion);
        destino=conexion.substr(posicion+1);
        unionn.push_back(origen);
        unionn.push_back(destino);
        uniones.push_back(unionn);
        unionn.clear(); 
        unionn.push_back(destino);
        unionn.push_back(origen);
        uniones.push_back(unionn);
    }
    archivo.close();
    return uniones;
}
bool buscador(string ordena){
    ifstream archivo;
    string ordenador;
    archivo.open("redes.txt",ios::in);
    if (archivo.fail()){
        cout<<"No se pudo abrir el archivo"<<endl;
        exit(1);
    }
    bool o;
    while(!archivo.eof()){
        archivo>>ordenador;
        if (ordenador==ordena){
            o=true;
        }
    }
    archivo.close();
    return o;
}
