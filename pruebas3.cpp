#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
using namespace std;

void escribir(double minuto, double profundidad);
int main(){
    double t=14;
    double h=15;
    escribir(t, h); 
}
void escribir(double minuto, double profundidad){
    ofstream archivo;
    archivo.open("nivelAgua.txt",ios::out |ios::app);
    if (archivo.fail()){
        cout<<"No se pudo abrir";
        exit(1);
    }
    archivo<<"tiempo: "<<fixed<<setprecision(1)<<minuto<<"min \t\t nivel de agua: "<<profundidad<<"cm"<<endl;
    archivo.close();
}