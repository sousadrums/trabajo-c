/*Primero se invocan los includes necesarios para la potenciación, los decimales y el formato científico*/
#include <iostream>
#include <math.h>
#include <string>
#include <iomanip>

using namespace std;

//Declaración de la constante global "N"
const int N=10000;
int main(){
    //petición del exponente y evaluación de si es > 0. 
    std::cout << "introduce el exponente del polinomio x^m: ";
    int m;
    std::cin >> m;
    if(m>0 ){
        //petición de los límites de integración y comprobación de que a<b.
        std::cout << "introduce el inicio del intervalo de integraci\242n: ";
        double a;
        std::cin >> a;
        std::cout << "introduce el final del intervalo de integraci\242n: ";
        double b;
        std::cin>> b;
        if (a<b)
        {
            //ejecución del programa. primero se calcula el ancho de cada rectángulo.
            //se declara la variable "suma" y se inicializa en 0.
            double ancho=(b-a)/N;
            double suma=0;
            /*este bucle permite sumar todos los rectángulos. Para ello, se emplea un iterador "i" entero, que empieza en 0 y acaba en "N". 
            Se crean las variables "xmenor" y "xmayor" que representan los valores inferior y superior de cada intervalo. Con ellas, se sustituye en la función "x^m"
            para obtener sus imágenes y se calcula la media. Esto dará la altura del rectángulo. Se calcula el area de cada rectangulo y, por recursividad, 
            se obtiene la suma de todos. Se guarda en la variable "suma"*/
            for (int i=0; i<N; i+=1)
            {
                double xmenor=a+i*ancho;
                double xmayor=xmenor+ancho;
                double alto=(std::pow(xmenor,m)+std::pow(xmayor,m))/2;
                double arearec=alto*ancho;
                suma=arearec+suma;
                //std::cout<<"Intervalo ("<<xmenor<<", "<<xmayor<<"). Altura del rect\240ngulo: "<<alto<<". Area: "<<arearec<<". Area acumulada: "<<suma<<"."<<endl;
                
            }
            /*A continuación se devuelven los resultados del programa. Se indican los parámetros elegidos, se muestran los cálculos de las sumas de Riemmann, 
            así como la integral por expresión analítica. Esta última se guarda en la variable "intdos". Por último, se devuelve la diferencia entre ambos valores.*/
            std::cout << "se hace la integral de x elevado a "<< m << " entre "<< a << " y " <<  b << std::endl;
            std::cout<<"la integral por sumas de Riemman es: "<<std::scientific<<std::setprecision(10)<<suma<<std::endl;
            double intdos=(std::pow(b,m+1)-std::pow(a,m+1))/(m+1);
            std::cout<<"la integral por la expresi\242n anal\241tica es: "<< intdos << std::endl;
            std::cout<<"la diferencia entre ambos valores es: "<< std::fabs(intdos-suma) << std::endl;

        }else{
            std::cout << "Error. El inicio del intervalo debe ser estrictamente menor que el final.";
        }
    }else{
        std::cout << "Error. El exponente debe ser natural.";
    }
    return 0;
 
    
}