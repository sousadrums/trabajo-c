#include <iostream>
#include <math.h>
#include <string>
#include <iomanip>

using namespace std;
const int N=10;
int main(){
    std::cout << "introduce el exponente del polinomio x^m: ";
    int m;
    std::cin >> m;
    if(m>0 ){
        std::cout << "introduce el inicio del intervalo de integraci\242n: ";
        double a;
        std::cin >> a;
        std::cout << "introduce el final del intervalo de integraci\242n: ";
        double b;
        std::cin>> b;
        if (a<b)
        {
            double ancho=(b-a)/N;
            double suma=0;
            for (int i=0; i<N; i+=1)
            {
                double xmenor=a+i*ancho;
                double xmayor=xmenor+ancho;
                double alto=(std::pow(xmenor,m)+std::pow(xmayor,m))/2;
                double arearec=alto*ancho;
                suma=arearec+suma;
                std::cout<<"Intervalo ("<<xmenor<<", "<<xmayor<<"). Altura del rect\240ngulo: "<<alto<<". Area: "<<arearec<<". Area acumulada: "<<suma<<"."<<endl;
            }
        }else{
            std::cout << "Error. El inicio del intervalo debe ser estrictamente menor que el final.";
        }
    }else{
        std::cout << "Error. El exponente debe ser natural.";
    }
    return 0;
 
    
}