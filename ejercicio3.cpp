#include <iostream>
#include <iomanip>
#include <math.h>
//liberia para gestionar archivos de texto
#include <fstream>
using namespace std;

void escribir(double minuto, double profundidad);
int main(){
    //declaración de variables
    double alpha=0.6;
    double g=980;
    double A=pow(0.5,2);
    double B=pow(100,2);
    int t0=0;
    int t=t0;
    double h;
    double hfin=0.1;
    double ho=250;
    double constante=-alpha*A/B*sqrt(2*980);
    h=ho;
    //Bucle que calcula el método de Euler.
    while(h>hfin){
        if(t%600==0){
            double minutos=t/60;
            escribir(minutos, h);
        }
        int deltat=1;
        //aquí se calcula el primer paso del método.
        double funcion=constante*sqrt(h);
        double hp=h+funcion*deltat;
        //Segundo paso del método. 
        h=h+(funcion+(constante*sqrt(hp)))/2*deltat;
        //se incrementa un segundo. 
        t++;
    }
    cout<<"El dep\242sito se ha vaciado en "<<t<<" segundos."<<endl;
    return 0;      
}
//función que crea o abre un archivo txt y escribe en él el instante y la altura del depósito
void escribir(double minuto, double profundidad){
    ofstream archivo;
    archivo.open("nivelAgua.txt",ios::out |ios::app);
    if (archivo.fail()){
        cout<<"No se pudo abrir";
        exit(1);
    }
    archivo<<"tiempo: "<<fixed<<setprecision(1)<<minuto<<"min \t\t nivel de agua: "<<profundidad<<"cm"<<endl;
    archivo.close();
}